# Dockerized Wordpress
Supports running in both **local** and **production** environment.

## Services included:
- nginx-proxy
- mariadb
- phpmyadmin
- wordpress

# Running Wordpress

### Local development
1 - Edit your /etc/hosts file and include:
```bash
127.0.0.1 wordpress.local
127.0.0.1 db.wordpress.local
```

2 - Start docker-compose:
```
docker-compose up -d
```

### Production
1 - Add nessecary configuration to `prod.docker-compose.yml`.

If you don't handle database and reverse proxy on the production server, copy the contents of `prod.docker-compose.yml` for easy setup.

2 - Start docker-compose in production mode:
```bash
docker-compose -f prod.docker-compose.yml up -d
```
